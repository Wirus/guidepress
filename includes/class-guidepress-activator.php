<?php

/**
 * Fired during plugin activation
 *
 * @link       https://hackergen.com
 * @since      1.0.0
 *
 * @package    Guidepress
 * @subpackage Guidepress/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Guidepress
 * @subpackage Guidepress/includes
 * @author     Wirus, Custom Protocol <admin@customprotocol.com>
 */
class Guidepress_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
