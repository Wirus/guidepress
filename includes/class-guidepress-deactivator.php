<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://hackergen.com
 * @since      1.0.0
 *
 * @package    Guidepress
 * @subpackage Guidepress/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Guidepress
 * @subpackage Guidepress/includes
 * @author     Wirus, Custom Protocol <admin@customprotocol.com>
 */
class Guidepress_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
