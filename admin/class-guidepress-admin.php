<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://hackergen.com
 * @since      1.0.0
 *
 * @package    Guidepress
 * @subpackage Guidepress/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Guidepress
 * @subpackage Guidepress/admin
 * @author     Wirus, Custom Protocol <admin@customprotocol.com>
 */
class Guidepress_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Guidepress_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Guidepress_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/guidepress-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Guidepress_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Guidepress_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/guidepress-admin.js', array( 'jquery' ), $this->version, false );

	}

	/**
	 * Creates a new "guide" custom post type
	 *
	 * @since 1.0.0
	 * @access public
	 * @uses register_post_type()
	 */
	public static function new_cpt_guide() {
		$plural = 'Guides';
		$single = 'Guide';
		$cpt_name = 'gp-guide';
		$opts = [
			'can_export' => TRUE,
			'capability_type' => 'post',
			'description' => '',
			'exclude_from_search' => FALSE,
			'has_archive' => TRUE,
			'hierarchical' => FALSE,
			'map_meta_cap' => TRUE,
			'menu_icon' => 'dashicons-welcome-learn-more',
			'menu_position' => 25,
			'public' => TRUE,
			'publicly_querable' => TRUE,
			'query_var' => TRUE,
			'register_meta_box_cb' => '',
			'rewrite' => ['slug' => 'gp-guide'],
			'show_in_admin_bar' => TRUE,
			'show_in_menu' => TRUE,
			'show_in_nav_menu' => TRUE,
			'show_in_rest' => true,
			'supports' => array('title', 'thumbnail', 'editor'),
			'labels' => [
				'add_new' => esc_html__( "Add New {$single}", 'wisdom' ),
				'add_new_item' =>esc_html__( "Add New {$single}", 'wisdom' ),
				'all_items' => esc_html__( $plural, 'wisdom' ),
				'edit_item' => esc_html__( "Edit {$single}" , 'wisdom' ),
				'menu_name' => esc_html__( $plural, 'wisdom' ),
				'name' => esc_html__( $plural, 'wisdom' ),
				'name_admin_bar' => esc_html__( $single, 'wisdom' ),
				'new_item' => esc_html__( "New {$single}", 'wisdom' ),
				'not_found' => esc_html__( "No {$plural} Found", 'wisdom' ),
				'not_found_in_trash' => esc_html__( "No {$plural} Found in Trash", 'wisdom' ),
				'parent_item_colon' => esc_html__( "Parent {$plural} :", 'wisdom' ),
				'search_items' => esc_html__( "Search {$plural}", 'wisdom' ),
				'singular_name' => 	esc_html__( $single, 'wisdom' ),
				'view_item' => esc_html__( "View {$single}", 'wisdom' )
			]
		];
		register_post_type( strtolower( $cpt_name ), $opts );
	}

	/**
	 * Creates a new "step" custom post type
	 *
	 * @since 1.0.0
	 * @access public
	 * @uses register_post_type()
	 */
	public static function new_cpt_step() {
		$plural = 'Steps';
		$single = 'Step';
		$cpt_name = 'gp-step';
		$opts = [
			'can_export' => TRUE,
			'capability_type' => 'post',
			'description' => '',
			'exclude_from_search' => FALSE,
			'has_archive' => TRUE,
			'hierarchical' => FALSE,
			'map_meta_cap' => TRUE,
			'menu_icon' => 'dashicons-clipboard',
			'menu_position' => 25,
			'public' => TRUE,
			'publicly_querable' => TRUE,
			'query_var' => TRUE,
			'register_meta_box_cb' => '',
			'rewrite' => ['slug' => 'gp-guide'],
			'show_in_admin_bar' => TRUE,
			'show_in_menu' => TRUE,
			'show_in_nav_menu' => TRUE,
			'show_in_rest' => true,
			'supports' => array('title', 'thumbnail', 'editor'),
			'labels' => [
				'add_new' => esc_html__( "Add New {$single}", 'wisdom' ),
				'add_new_item' =>esc_html__( "Add New {$single}", 'wisdom' ),
				'all_items' => esc_html__( $plural, 'wisdom' ),
				'edit_item' => esc_html__( "Edit {$single}" , 'wisdom' ),
				'menu_name' => esc_html__( $plural, 'wisdom' ),
				'name' => esc_html__( $plural, 'wisdom' ),
				'name_admin_bar' => esc_html__( $single, 'wisdom' ),
				'new_item' => esc_html__( "New {$single}", 'wisdom' ),
				'not_found' => esc_html__( "No {$plural} Found", 'wisdom' ),
				'not_found_in_trash' => esc_html__( "No {$plural} Found in Trash", 'wisdom' ),
				'parent_item_colon' => esc_html__( "Parent {$plural} :", 'wisdom' ),
				'search_items' => esc_html__( "Search {$plural}", 'wisdom' ),
				'singular_name' => 	esc_html__( $single, 'wisdom' ),
				'view_item' => esc_html__( "View {$single}", 'wisdom' )
			]
		];
		register_post_type( strtolower( $cpt_name ), $opts );
	}

	public function enqueue_gutenberg_blocks_scripts() {

		wp_enqueue_script(
			$this->plugin_name . '-gutenberg-blocks',
			GUIDEPRESS_ROOT_URL . 'dist/blocks.build.js',
			array( 'wp-blocks', 'wp-i18n', 'wp-element', 'wp-editor' )
		);

	}

	public function register_gutenberg_blocks() {

		register_block_type( 'guidepress/step-block', [
			'editor_script' => $this->plugin_name
		] );

	}

}
